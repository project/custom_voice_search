Custom Voice Search

This module adds the ability to any form having search field to add voice support.

Configure:
Enable like any other module.
Go to: /admin/config/custom_voice_search/customvoicesearch
Add Form id on which voice search needs to be implemented like: "user_login_form" not "user-login-form"
Add Input id like: "edit-keys" not "edit_keys"
Add Input field name attribute like: keys or name etc.

As Voice Search functionality is working on Chrome Browser, So this will work for Chrome only.